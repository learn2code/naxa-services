import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Initiatives, Navbar, Scroller, Services } from "./components";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: "SERVICE_FETCH" });
  }, []);

  return (
    <div>
      <Initiatives />
      <Navbar />
      <Services />
      <Scroller />
    </div>
  );
}

export default App;
