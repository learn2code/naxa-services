import { ChevronUpIcon } from "@heroicons/react/24/solid";
import React, { useEffect, useState } from "react";

export const Scroller = () => {
  const [showScrollButton, setShowScrollButton] = useState(false);
  useEffect(() => {
    const handleScrollButtonVisibility = () => {
      window.pageYOffset > 100
        ? setShowScrollButton(true)
        : setShowScrollButton(false);
    };
    window.addEventListener("scroll", handleScrollButtonVisibility);
    return () => {
      window.removeEventListener("scroll", handleScrollButtonVisibility);
    };
  }, []);

  return (
    <>
      {showScrollButton && (
        <div className="fixed bottom-10 right-10">
          <button
            className="bg-slate-200 rounded-full p-4"
            onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
          >
            <ChevronUpIcon className="text-blue-500 w-6 h-6" />
          </button>
        </div>
      )}
    </>
  );
};
