import React from "react";
import firstImg from "../assets/images/first.png";
import secondImg from "../assets/images/second.png";
import secondPhoneImg from "../assets/images/second-phone.png";
import secondIconImg from "../assets/images/second-icon.svg";
import thirdImg from "../assets/images/third.png";
import thirdimgImg from "../assets/images/third-img.png";
import thirdIconImg from "../assets/images/third-icon.svg";

export const Services = () => {
  return (
    <div className="relative flex justify-center ">
      <div className="absolute top-0 -z-50">
        <img src={firstImg} alt="first" />
        <img src={secondImg} alt="second" />
        <img src={thirdImg} alt="third" />
      </div>
      <div className="flex flex-col w-3/4">
        <div className="flex flex-col space-y-16 py-16 h-[600px]">
          <div className="text-yellow-500 font-semibold">SERVICES</div>
          <div className="text-5xl">
            At <span className="text-blue-700">NAXA</span>, we work on{" "}
            <span className="text-blue-700">ideas</span>; ideas that
            <br /> can provide{" "}
            <span className="text-blue-700">simple solutions</span> to
            <br /> <span className="text-blue-700">complex problems</span>.
          </div>
          <div className="text-lg font-semibold">
            We work as a team to generate, explore, build and validate ideas. We
            also
            <br /> contextualize innovations around the world to find the best
            fitting solutions to
            <br /> local problems.
          </div>
        </div>
        <div className="flex flex-row items-center h-[600px] my-12">
          <img
            src={secondPhoneImg}
            alt="second-phone"
            className="w-1/2 px-32 py-8"
          />
          <div className="flex flex-col space-y-8 py-16">
            <img src={secondIconImg} alt="second-icon" className="w-16 h-16" />
            <div className="font-semibold text-3xl">
              Software & Apps Development
            </div>
            <div className="text-base">
              We build digital applications to provide digital solutions that
              contribute to efficient
              <br /> data collection and visualization focusing mainly on
              geodata collection and
              <br /> visualization.
            </div>
            <div className="text-base bg-indigo-100 p-6 rounded-sm">
              We develop digital software solutions ensuring smooth performances
              and user experiences across all modern platforms and devices.
              Whether it's a digital application for a private organization or a
              set of software solutions for development organizations, we offer
              support for the full lifecycle of the software development process
              that includes system design, development, testing, deployment,
              handover, and support, these activities are conducted with
              world-class product design and development practices. Our services
              include building custom Android and iOS applications, web
              applications, and tools. We are particularly strong in customizing
              open-source applications such as ODKCollect and KoboCollect and
              building advanced GIS Applications.
            </div>
          </div>
        </div>
        <div className="flex flex-row items-center h-[600px] my-12">
          <div className="flex flex-col space-y-8 py-16">
            <img src={thirdIconImg} alt="second-icon" className="w-16 h-16" />
            <div className="font-semibold text-3xl">GIS Mapping & Analysis</div>
            <div className="text-base">
              The core domain of the company is digital mapping. We work in
              desktop, web, and
              <br /> mobile mapping and produce accurate and visually appealing
              digital maps.
            </div>
            <div className="text-base bg-indigo-100 p-6 rounded-sm">
              We provide GIS-based geodata management and analysis services
              ranging from geographical data handling to spatial analysis for
              informed decision-making. We emphasize digital mapping and provide
              services that allow users to create maps in a digital medium,
              interact over the maps, and draw meaningful information via
              map-based queries. We specialize in the development of online GIS
              applications to visualize and analyze spatial datasets. We develop
              location-based digital mapping applications and services for both
              web and mobile platforms. We also develop geo-portals that allow
              the organization and even government bodies to create and maintain
              a map-based inventory of their works, projects, or any other
              datasets that can be associated with specific locations
            </div>
          </div>
          <img
            src={thirdimgImg}
            alt="second-phone"
            className="w-1/2 px-40 py-8"
          />
        </div>
      </div>
    </div>
  );
};
