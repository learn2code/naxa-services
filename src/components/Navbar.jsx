import { ArrowRightIcon, ChevronDownIcon } from "@heroicons/react/24/solid";
import React, { useEffect, useRef, useState } from "react";
import NaxaImg from "../assets/images/naxa.png";

const menuItems = [
  {
    title: "Services",
    link: "#services",
  },
  {
    title: "Portfolio",
    link: "#portfolio",
  },
  {
    title: "Company",
    link: "#company",
  },
  {
    title: "Events & Media",
    link: "#events-media",
  },
  {
    title: "Blogs",
    link: "#blogs",
  },
];

export const Navbar = () => {
  const dropdownRef = useRef(null);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const handleCloseDropdown = () => {
    setIsDropdownOpen(false);
  };

  return (
    <div className="flex flex-row justify-center w-full pt-2">
      <div className="flex flex-row justify-between w-3/4">
        <a className="font-bold text-[30px]" href="#naxa">
          <img src={NaxaImg} className="h-10" alt="naxa-logo" />
        </a>
        <div className="flex flex-row items-center space-x-12">
          {menuItems.map((item, index) =>
            index === 1 ? (
              <div className="relative flex items-center space-x-1" key={index} ref={dropdownRef}>
                <button onMouseOver={() => setIsDropdownOpen(true)}>
                  {item.title}
                </button>
                <ChevronDownIcon className="w-3 h-3" />
                {isDropdownOpen && (
                  <div
                    className="flex flex-col absolute top-10 shadow-xl border border-gray-100 w-60"
                    onMouseLeave={handleCloseDropdown}
                  >
                    <a
                      href="#general"
                      className="px-6 py-4 hover:bg-yellow-300"
                    >
                      General
                    </a>
                    <a
                      href="#international"
                      className="px-6 py-4 hover:bg-yellow-300"
                    >
                      International
                    </a>
                  </div>
                )}
              </div>
            ) : (
              <div key={index}>
                <button>{item.title}</button>
              </div>
            )
          )}
        </div>

        <div className="flex items-center font-semibold text-sm py-2 justify-center w-40 space-x-2 bg-yellow-300 hover:text-blue-700 hover:space-x-4">
          <a href="#lets-talk" alt="lets-talk">
            Let's Talk
          </a>
          <ArrowRightIcon className="w-4 h-4" />
        </div>
      </div>
    </div>
  );
};
