import React from 'react'
import { ArrowRightIcon } from '@heroicons/react/24/solid'

export const Initiatives = () => {
  return (
    <div className='h-10 bg-yellow-300 text-sm flex justify-center items-center space-x-2 font-semibold underline hover:text-blue-700 hover:space-x-4'>
        <a href='#initiative'>We Have Been Working On Several Voluntary Initiatives During The COVID-19 Pandemic. Check Them Out</a>
        <ArrowRightIcon className='w-4 h-4' />
    </div>
  )
}