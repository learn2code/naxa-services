import { API_ACTIONS } from "../actions/api"
const reducer = (data = {
    "loading": false,
    "data": {},
    "errormsg": ""
}, action) => {
    if (action.type === API_ACTIONS.LOADING_API) {
        data = {
            ...data,
            "loading": true
        }
    } else if (action.type === API_ACTIONS.SUCCESS_API) {
        data = Object.assign({}, data, {
            data: action.payload,
            loading: false
        })
    } else if (action.type === API_ACTIONS.ERROR_API) {
        data = {
            "loading": false,
            "data": {},
            "errormsg": action.payload
        }
    }
    return data
}
export default reducer