import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import api from './reducers/api'
import { naxaSaga } from './saga'

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(combineReducers({
    api,
}), {}, applyMiddleware(sagaMiddleware))

function* rootSaga() {
    yield all([
        naxaSaga(),
    ])
}
sagaMiddleware.run(rootSaga)