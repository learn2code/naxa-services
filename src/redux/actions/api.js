const LOADING_API = "api_loading"
const SUCCESS_API = "api_success"
const ERROR_API = "api_error"
export const API_ACTIONS = {
    LOADING_API,
    SUCCESS_API,
    ERROR_API
}
export function loadingAPIAction() {
    return {
        type: LOADING_API
    }
}
export function completedAPIAction(payload) {
    return {
        type: SUCCESS_API,
        payload: payload
    }
}
export function errorAPIAction(error) {
    return {
        type: ERROR_API,
        payload: error
    }
}