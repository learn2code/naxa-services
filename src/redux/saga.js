import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { loadingAPIAction, errorAPIAction, completedAPIAction } from "./actions/api"

const getservicesapi = async() => {
    return await fetch("https://admin.naxa.com.np/api/services")
        .then((data) => {
            return data.json().then((value) => {
                return value
            })
        })
}

function* fetchServices(action) {
    try {
        yield put(loadingAPIAction())
        const services = yield call(getservicesapi, action.payload);
        yield put(completedAPIAction(services));
    } catch (e) {
        yield put(errorAPIAction(e));
    }
}

export function* naxaSaga() {
    yield takeEvery("SERVICE_FETCH", fetchServices);
}